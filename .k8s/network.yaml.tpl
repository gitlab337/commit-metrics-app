apiVersion: v1
kind: Service
metadata:
  name: ${APPLICATION_NAME}
  namespace: ${APPLICATION_NAMESPACE}
  labels:
    app.kubernetes.io/name: ${APPLICATION_NAME}
    app.kubernetes.io/component: api
    app.kubernetes.io/part-of: ${APPLICATION_NAME}
spec:
  selector:
    app.kubernetes.io/name: ${APPLICATION_NAME}
    app.kubernetes.io/component: api
  type: ClusterIP
  ports:
  - name: ${APPLICATION_NAME}
    protocol: TCP
    port: 80
    targetPort: 8080
      
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ${APPLICATION_NAME}
  annotations:
    kubernetes.io/ingress.class: nginx
  namespace: ${APPLICATION_NAMESPACE}
spec:
  rules:
  - host: ${LOAD_BALANCER_DNS}
    http:
      paths:
      - path: /
        backend:
          serviceName: ${APPLICATION_NAME}
          servicePort: 80
