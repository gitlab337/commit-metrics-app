apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${APPLICATION_NAME} 
  namespace: ${APPLICATION_NAMESPACE}
  labels:
    app.kubernetes.io/name: ${APPLICATION_NAME}
    app.kubernetes.io/component: api
    app.kubernetes.io/part-of: ${APPLICATION_NAME}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: ${APPLICATION_NAME}
  replicas: 3
  strategy:
    rollingUpdate:
      maxSurge: 50%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ${APPLICATION_NAME}
        app.kubernetes.io/component: api
        app.kubernetes.io/part-of: ${APPLICATION_NAME}
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: "app.kubernetes.io/name" 
                  operator: In
                  values:
                    - ${APPLICATION_NAME}
              topologyKey: "kubernetes.io/hostname"
      containers:
      - name: ${APPLICATION_NAME}
        image: ${APPLICATION_IMAGE}
        imagePullPolicy: IfNotPresent
        resources:
          requests:
            cpu: 100m
            memory: 200Mi
          limits:
            cpu: 200m
            memory: 400Mi
        livenessProbe:
          httpGet:
            path: /api/heatlh
            port: 8080
            httpHeaders:
              - name: Host
                value: localhost 
          initialDelaySeconds: 40
          timeoutSeconds: 20
          successThreshold: 1
          failureThreshold: 3
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: /api/heatlh
            port: 8080
            httpHeaders:
              - name: Host
                value: localhost 
          initialDelaySeconds: 40
          timeoutSeconds: 20
          successThreshold: 1
          failureThreshold: 3
          periodSeconds: 10
        env:
        - name: GITLAB_API_KEY
          valueFrom:
            secretKeyRef:
              name: ${APPLICATION_SECRET_NAME}
              key: ${APPLICATION_SECRET_KEY}
        ports:
        - containerPort: 8080
          name: ${APPLICATION_NAME}

---
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  namespace: ${APPLICATION_NAMESPACE}
  name: ${APPLICATION_NAME}
  labels:
    app.kubernetes.io/name: ${APPLICATION_NAME}
    app.kubernetes.io/component: api
    app.kubernetes.io/part-of: ${APPLICATION_NAME}         
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${APPLICATION_NAME}
  minReplicas: 3
  maxReplicas: 6
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 65
    - type: Resource
      resource:
        name: memory
        target:
          type: Utilization
          averageUtilization: 80
---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: ${APPLICATION_NAME}
  namespace: ${DEPLOYMENT_NAMESPACE}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: ${APPLICATION_NAME}
  minAvailable: 25%
