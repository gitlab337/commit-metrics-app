module gitlab.com/lmendes_94/commit-metrics

go 1.15

require (
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.51.1
	gotest.tools v2.2.0+incompatible
)
