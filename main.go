package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/lmendes_94/commit-metrics/health"
	"gitlab.com/lmendes_94/commit-metrics/metrics"
)

var (
	bindHost  string
	bindPort  string
	GitlabUrl string
)

func init() {
	flag.StringVar(&bindHost, "host", "0.0.0.0", "api host addr")
	flag.StringVar(&bindPort, "port", "8080", "api port")
	flag.StringVar(&GitlabUrl, "gitlab-url", "https://gitlab.com/", "gitlab url")
}

func main() {

	flag.Parse()

	gialabClient, err := gitlab.NewClient(os.Getenv("GITLAB_API_KEY"), gitlab.WithBaseURL(GitlabUrl))

	if err != nil {
		fmt.Errorf("an error occurs when try to create a gitlab client %w", err)
		os.Exit(1)
	}

	router := mux.NewRouter()

	router.Use(mux.CORSMethodMiddleware(router))
	router.Use(func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			rw.Header().Set("Content-Type", "application/json")
			h.ServeHTTP(rw, r)
		})
	})

	health.HealthApi(router)
	metrics.CommitMetricsApi(router, metrics.NewRepository(gialabClient))

	server := http.Server{
		Addr:    fmt.Sprintf("%s:%s", bindHost, bindPort),
		Handler: router,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	server.Shutdown(ctx)
	log.Println("graceful shutdown rules!!!")
}
