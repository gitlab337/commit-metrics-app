FROM golang:1.15-alpine AS builder

WORKDIR /app

COPY . .

RUN go get -v ./... \
	&& GOOS=linux go build -race -o server  

# ============================================================================== #

FROM alpine:3.14 
RUN apk --no-cache add ca-certificates

WORKDIR /

COPY --from=builder /app/server .

EXPOSE 8080
CMD ["./server"]
