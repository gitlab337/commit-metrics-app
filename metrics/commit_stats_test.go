package metrics

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func createGitlabCommitMock(commitId, email string, total int) *gitlab.Commit {
	return &gitlab.Commit{
		ID:             commitId,
		CommitterEmail: email,
		Stats: &gitlab.CommitStats{
			Total: total,
		},
	}
}

func TestShouldGroupCommitByAuthor(t *testing.T) {
	commitCollection := []*gitlab.Commit{
		createGitlabCommitMock("db8e1693e2c292a5fc8b463b25eaf957fbcf7d09", "brucewayne@gmail.com", 15),
		createGitlabCommitMock("26ad0ee7735c531b8de5ba0885ffd85559f5f529", "wallywest@hotmail.com", 55),
		createGitlabCommitMock("26ad0ee7735c531b8de5ba0885ffd85559f5f529", "wallywest@hotmail.com", 20),
		createGitlabCommitMock("7849247951caf703b28fb73456121b2a4d88611c", "arthurcoury@outlook.com", 190),
		createGitlabCommitMock("2273b1598492a9001cd6d15b42c693591ce7ddcb", "brucewayne@gmail.com", 220),
		createGitlabCommitMock("e98dc4b81d6467a706754c5929702356e8c1e7b8", "clarkkent@yahoo.com", 3),
		createGitlabCommitMock("1710b6ce93047d2c2f0997ac59b94b96ca4a12a4", "wallywest@hotmail.com", 35),
		createGitlabCommitMock("d8d5ae3ada26b377b160bc5751b835d21b99011f", "brucewayne@gmail.com", 70),
		createGitlabCommitMock("ecc4fea33a35cb8df58b23781319bd966013130e", "clarkkent@yahoo.com", 400),
		createGitlabCommitMock("db8e1693e2c292a5fc8b463b25eaf957fbcf7d09", "brucewayne@gmail.com", 22),
	}

	expectedResult := []CommitStats{
		{
			CommiterEmail:           "brucewayne@gmail.com",
			CommitQuantity:          4,
			CommitChangedTotalLines: 327,
		},
		{
			CommiterEmail:           "wallywest@hotmail.com",
			CommitQuantity:          3,
			CommitChangedTotalLines: 110,
		},
		{
			CommiterEmail:           "clarkkent@yahoo.com",
			CommitQuantity:          2,
			CommitChangedTotalLines: 403,
		},
		{
			CommiterEmail:           "arthurcoury@outlook.com",
			CommitQuantity:          1,
			CommitChangedTotalLines: 190,
		},
	}

	groupedCommits := getCommitStatsByAuthor(commitCollection)

	for i := 0; i < len(groupedCommits); i++ {
		assert.Equal(t, expectedResult[i].CommiterEmail, groupedCommits[i].CommiterEmail)
		assert.Equal(t, expectedResult[i].CommitQuantity, groupedCommits[i].CommitQuantity)
		assert.Equal(t, expectedResult[i].CommitChangedTotalLines, groupedCommits[i].CommitChangedTotalLines)
	}
}
