package metrics

import (
	"sort"

	"github.com/xanzy/go-gitlab"
)

type CommitStats struct {
	CommiterEmail           string `json:"commiter_email"`
	CommitQuantity          int    `json:"commit_quantity"`
	CommitChangedTotalLines int    `json:"changed_lines"`
}

func getCommitStatsByAuthor(commits []*gitlab.Commit) []CommitStats {
	statsMapper := make(map[string]*CommitStats)

	for _, commit := range commits {
		storedCommit, hasCommit := statsMapper[commit.CommitterEmail]

		if !hasCommit {
			statsMapper[commit.CommitterEmail] = &CommitStats{
				CommitQuantity:          1,
				CommitChangedTotalLines: commit.Stats.Total,
				CommiterEmail:           commit.CommitterEmail,
			}
		} else {
			storedCommit.CommitQuantity += 1
			storedCommit.CommitChangedTotalLines += commit.Stats.Total
		}
	}

	commitStatsByAuthor := make([]CommitStats, 0, len(statsMapper))

	for _, v := range statsMapper {
		commitStatsByAuthor = append(commitStatsByAuthor, *v)
	}

	sort.Slice(commitStatsByAuthor, func(i, j int) bool {
		return commitStatsByAuthor[j].CommitQuantity < commitStatsByAuthor[i].CommitQuantity
	})

	return commitStatsByAuthor
}
