package metrics

import (
	"github.com/xanzy/go-gitlab"
)

type CommitCollector interface {
	GetCommits(projectID interface{}) ([]*gitlab.Commit, error)
}

type commitCollector struct {
	client *gitlab.Client
}

func (c *commitCollector) GetCommits(projectID interface{}) ([]*gitlab.Commit, error) {
	gitlabCommitCollection := make([]*gitlab.Commit, 0)

	gitlabRequestOpts := &gitlab.ListCommitsOptions{
		All:       gitlab.Bool(true),
		WithStats: gitlab.Bool(true),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {

		commits, res, err := c.client.Commits.ListCommits(projectID, gitlabRequestOpts)

		if err != nil {
			return nil, err
		}

		gitlabCommitCollection = append(gitlabCommitCollection, commits...)

		if res.CurrentPage >= res.TotalPages {
			break
		}

		gitlabRequestOpts.Page = res.NextPage
	}

	return gitlabCommitCollection, nil
}

func NewRepository(client *gitlab.Client) CommitCollector {
	return &commitCollector{
		client: client,
	}
}
