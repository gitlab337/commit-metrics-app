package metrics

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/xanzy/go-gitlab"
)

type mockRepository struct {
	mock.Mock
}

func (m *mockRepository) GetCommits(projectID interface{}) ([]*gitlab.Commit, error) {
	args := m.Called(projectID)
	return args.Get(0).([]*gitlab.Commit), args.Error(1)
}

func TestShouldReturnError(t *testing.T) {
	expectedOutput := strings.Builder{}
	expectedOutput.WriteString(`{"message":"Generic Error"}`)
	expectedOutput.WriteString("\n")

	repository := new(mockRepository)
	repository.On("GetCommits", mock.Anything).Return(make([]*gitlab.Commit, 0), errors.New("Generic Error"))

	handler := commitMetricsHandler{repository: repository}

	req, err := http.NewRequest("GET", "/metrics/27387655999", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router := mux.NewRouter()
	router.Handle("/metrics/{projectID}", handler).Methods("GET")
	router.ServeHTTP(rr, req)

	repository.AssertExpectations(t)

	assert.Equal(t, 500, rr.Code)
	assert.Equal(t, expectedOutput.String(), rr.Body.String())
}

func TestShouldReturnErrorWhenProjectIDIsInvalid(t *testing.T) {
	expectedOutput := strings.Builder{}
	expectedOutput.WriteString(`{"message":"projectID parameter should be a integer value!"}`)
	expectedOutput.WriteString("\n")

	repository := new(mockRepository)
	repository.On("GetCommits", mock.Anything).Return(make([]*gitlab.Commit, 0), nil)

	handler := commitMetricsHandler{repository: repository}

	req, err := http.NewRequest("GET", "/metrics/xpto", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router := mux.NewRouter()
	router.Handle("/metrics/{projectID}", handler).Methods("GET")
	router.ServeHTTP(rr, req)

	repository.AssertNotCalled(t, "GetCommit")

	assert.Equal(t, 400, rr.Code)
	assert.Equal(t, expectedOutput.String(), rr.Body.String())
}

func TestShouldReturnGroupedGitlabCommitsStats(t *testing.T) {
	expectedResult := strings.Builder{}
	expectedResult.WriteString(`{"data":[{"commiter_email":"brucewayne@gmail.com","commit_quantity":1,"changed_lines":1275}]}`)
	expectedResult.WriteString("\n")

	repository := new(mockRepository)
	repository.On("GetCommits", mock.Anything).Return([]*gitlab.Commit{
		createGitlabCommitMock("2756b0517e8335c3933e1cb2cc0aa1523b712e1b", "brucewayne@gmail.com", 1275),
	}, nil)

	handler := commitMetricsHandler{repository: repository}

	req, err := http.NewRequest("GET", "/metrics/123", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router := mux.NewRouter()
	router.Handle("/metrics/{projectID}", handler).Methods("GET")
	router.ServeHTTP(rr, req)

	repository.AssertExpectations(t)

	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, expectedResult.String(), rr.Body.String())
}
