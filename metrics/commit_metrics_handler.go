package metrics

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type commitMetricsHandler struct {
	repository CommitCollector
}

func (m commitMetricsHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	value := mux.Vars(req)["projectID"]

	if _, err := strconv.Atoi(value); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"message": "projectID parameter should be a integer value!"})
		return
	}

	commits, err := m.repository.GetCommits(value)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"message": err.Error()})
		return
	}

	commitStats := getCommitStatsByAuthor(commits)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string][]CommitStats{"data": commitStats})
}

func CommitMetricsApi(router *mux.Router, repository CommitCollector) {
	router.Handle("/api/metrics/{projectID}", commitMetricsHandler{repository: repository}).Methods("GET")
}
