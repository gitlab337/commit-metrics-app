package health

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShouldReturnOkWhenHealthCheckIsCalled(t *testing.T) {
	expected := strings.Builder{}
	expected.WriteString(`{"health":"Ok"}`)
	expected.WriteString("\n")

	req, err := http.NewRequest("GET", "/api/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(healthCheck)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, expected.String(), rr.Body.String())
}
