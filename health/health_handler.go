package health

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

//healthcheck example
func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]string{"health": "Ok"})
}

func HealthApi(router *mux.Router) {
	router.HandleFunc("/api/health", healthCheck).Methods("GET")
}
